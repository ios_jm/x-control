//
//  AppDelegate.h
//  XControlx
//
//  Created by Tiago Paluch on 18/09/14.
//  Copyright (c) 2014 Tiago Paluch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

