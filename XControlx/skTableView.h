//
//  skTableView.h
//  XControlx
//
//  Created by Tiago Paluch on 20/09/14.
//  Copyright (c) 2014 Tiago Paluch. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "SkTableViewCell.h"
#import "GameClientAppDelegate.h"
#import "GameScene.h"

@interface skTableView : SKScene <SkTableViewCellDelegate, GamejoyDelegate>
@property (strong) GameClientAppDelegate* servidor;

- (void)UpdateCels;
- (void)Click:(int)row;
- (void)Conection:(bool)status;
- (void)setConfig:(bool)status;
@end
