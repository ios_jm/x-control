//
//  Dbutton.m
//  Joystick
//
//  Created by Tiago Paluch on 24/07/14.
//  Copyright (c) 2014 Tiago Paluch. All rights reserved.
//

#import "Dbutton.h"

@implementation Dbutton {
    // Touch handling
    BOOL isTouching;
    CFMutableDictionaryRef trackedTouches;
    UIColor* _colorF;
    UIColor* _colorS;
    SKSpriteNode* bt1;
    SKSpriteNode* bt2;
    BOOL first;
}
@synthesize botao;
@synthesize ativo;
- (instancetype)initWithRect:(CGRect)rect andStylo:(int)sty andLabel:(NSString *)blabel {
    if (( self = [super init] )) {
        _Stylo = sty;
        ativo = NO;
        isTouching = NO;
        _rect = rect;
        trackedTouches = CFDictionaryCreateMutable(kCFAllocatorDefault, 0, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
        _colorF = [UIColor whiteColor];
        _colorS = [UIColor blackColor];
        _texto = blabel;
        first = YES;

    }
    return self;
}
- (instancetype) initWithRect:(CGRect)rect andColorFill:(UIColor *)colorF andColorStroke:(UIColor *)colorS {
    if (( self = [super init] )) {
        _Stylo = 0;
        ativo = NO;
        isTouching = NO;
        _rect = rect;
        trackedTouches = CFDictionaryCreateMutable(kCFAllocatorDefault, 0, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
        _colorF = colorF;
        _colorS = colorS;
        _texto = @"";
        first = YES;

    }
    return self;
}
- (instancetype) initWithRect:(CGRect)rect andColorFill:(UIColor *)colorF andColorStroke:(UIColor *)colorS label:(NSString*)blabel{
    if (( self = [super init] )) {
        ativo = NO;
        isTouching = NO;
        _rect = rect;
        trackedTouches = CFDictionaryCreateMutable(kCFAllocatorDefault, 0, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
        _colorF = colorF;
        _colorS = colorS;
        _texto = blabel;
        
        
        first = YES;
        
    }
    return self;
}
- (void)iniciar {
    if (!first ) {
        [self removeAllChildren];
    }
    first = NO;
    self.bRadius = CGRectGetWidth(_rect) / 2;
    self.position = _rect.origin;
    
    if (_Stylo==-1) {
        self.hidden=YES;
    } else {
        self.hidden=NO;
    }
    
    botao = [SKShapeNode node];
    botao.fillColor = _colorF;
    botao.strokeColor = _colorS;
    botao.lineWidth = 2.0f;
    CGRect mrect = CGRectMake(0, 0, 0, 0);
    mrect.size = _rect.size;
    botao.path = CGPathCreateWithEllipseInRect(mrect, nil);
    
    
    
    NSString* img = @"";
    
    switch (_Stylo) {
        case 1:
            img = @"sci-fi";
            break;
        case 2:
            img = @"arcade";
            break;
        case 3:
            img = @"rustico";
            break;
        default:
            img = @"sci-fi";
            break;
    }
    
    bt1 = [[SKSpriteNode alloc] initWithImageNamed:[NSString stringWithFormat:@"%@-button-01A",img]];
    bt2 = [[SKSpriteNode alloc] initWithImageNamed:[NSString stringWithFormat:@"%@-button-01P",img]];
    
    bt1.position = CGPointMake(_rect.size.width/2, _rect.size.height/2);
    bt2.position = CGPointMake(_rect.size.width/2, _rect.size.height/2);
    botao.position = CGPointMake(0, 0);
    
    
    bt1.size = _rect.size;
    bt2.size = _rect.size;
    
    //SKShapeNode* sktemp = [[SKShapeNode alloc] init];
    //sktemp.path = CGPathCreateWithRect(_rect, nil);
    
    bt2.hidden = YES;
    if (_Stylo==0) {
        bt1.hidden = YES;
    } else {
        bt1.hidden = NO;
    }
    
    

    //[self addChild:sktemp];
    
    
    _title = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
    [_title setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
    [_title setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeCenter];
    [[self title] setText:_texto];
    [[self title] setPosition:CGPointMake(_rect.size.width/2, _rect.size.height/2)];
    
    
    //if (first ) {
        [self addChild:botao];
        [self addChild:bt1];
        [self addChild:bt2];
        [self addChild:_title];
     ///   first = NO;
    //}
    self.position = CGPointMake(CGRectGetMinX(self.framepai)+_rect.origin.x, CGRectGetMinY(self.framepai)+_rect.origin.y);
    self.userInteractionEnabled = YES;
}
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [touches enumerateObjectsUsingBlock:^(id obj, BOOL *stop) {
        
        // First determine if the touch is within the boundries of the DPad
        UITouch *touch = (UITouch *)obj;
        
        CGPoint location = [touch locationInNode:self];
        
        location = CGPointMake(location.x - self.bRadius, location.y - self.bRadius);
        
        if ( !(location.x < -self.bRadius || location.x > self.bRadius || location.y < -self.bRadius || location.y > self.bRadius) ) {
            ativo = YES;
            isTouching = YES;
            if (_Stylo>0) {
                bt2.hidden = NO;
            }
            CFDictionarySetValue(trackedTouches, (__bridge void *)touch, (__bridge void *)touch);
            //UIColor* cor = botao.fillColor;
            //[cor.]
        }
    }];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if ( isTouching )
    {
        [touches enumerateObjectsUsingBlock:^(id obj, BOOL *stop) {
            
            // Determine if this is a tracked touch
            UITouch *touch = (UITouch *) CFDictionaryGetValue(trackedTouches, (__bridge void *)(UITouch *)obj);
            
            if ( touch != NULL )
            {
                ativo = NO;
                bt2.hidden = YES;
                CFDictionaryRemoveValue(trackedTouches, (__bridge void *)touch);
            }
            
        }];
    }
}


- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchesEnded:touches withEvent:event];
}
@end
