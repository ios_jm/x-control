//
//  NSMutableArray+QueueStack.h
//  XControlx
//
//  Created by Tiago Paluch on 30/10/14.
//  Copyright (c) 2014 Tiago Paluch. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HsuQueue : NSObject {
    NSMutableArray* m_array;
}

- (void)enqueue:(id)anObject;
- (id)dequeue;
- (void)clear;

@property (nonatomic, readonly) int count;

@end