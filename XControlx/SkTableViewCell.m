//
//  SkTableViewCell.m
//  XControlx
//
//  Created by Tiago Paluch on 20/09/14.
//  Copyright (c) 2014 Tiago Paluch. All rights reserved.
//

#import "SkTableViewCell.h"

@implementation SkTableViewCell{
    // Touch handling
    BOOL isTouching;
    CFMutableDictionaryRef trackedTouches;
}

- (instancetype)initWithNome:(NSString *)Nome andLinha:(int)Linha andRect:(CGRect)rect{
    self = [super init];
    if (self) {
        _texto = Nome;
        _linha = Linha;
        _retangulo = rect;
        isTouching = NO;
        trackedTouches = CFDictionaryCreateMutable(kCFAllocatorDefault, 0, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
        

        
        
        SKLabelNode* title = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
        [self addChild:title];
        [title setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
        [title setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeCenter];
        [title setText:_texto];
        [title setPosition:CGPointMake(rect.size.width/2, rect.size.height/2)];
        
        SKShapeNode* quadrado = [SKShapeNode node];
        [self addChild:quadrado];
        quadrado.fillColor = [UIColor blueColor];
        quadrado.strokeColor = [UIColor blueColor];
        quadrado.lineWidth = 2.0f;
        quadrado.path = CGPathCreateWithRect(rect, nil);
    }
    return self;
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];

        
        
        if ( !(location.x < -self.retangulo.origin.x || location.x > (self.retangulo.origin.x + self.retangulo.size.width) || location.y < -self.retangulo.origin.y || location.y > (self.retangulo.origin.y + self.retangulo.size.height)) ) {
            isTouching = YES;
            CFDictionarySetValue(trackedTouches, (__bridge void *)touch, (__bridge void *)touch);
            
            if ([self.delegate respondsToSelector:@selector(Click:)]) {
                [self.delegate Click:self.linha];
            }
        }

    }
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if ( isTouching )
    {
        [touches enumerateObjectsUsingBlock:^(id obj, BOOL *stop) {
            UITouch *touch = (UITouch *) CFDictionaryGetValue(trackedTouches, (__bridge void *)(UITouch *)obj);
            if ( touch != NULL )
            {
                CFDictionaryRemoveValue(trackedTouches, (__bridge void *)touch);
            }
            
        }];
    }
}


- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchesEnded:touches withEvent:event];
}
@end
