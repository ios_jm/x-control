//
//  GameViewController.h
//  XControlx
//

//  Copyright (c) 2014 Tiago Paluch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import "GameClientAppDelegate.h"

@interface GameViewController : UIViewController
@property (strong) GameClientAppDelegate* servidor;
@end
