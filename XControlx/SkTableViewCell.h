//
//  SkTableViewCell.h
//  XControlx
//
//  Created by Tiago Paluch on 20/09/14.
//  Copyright (c) 2014 Tiago Paluch. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
@class SkTableViewCell;
@protocol SkTableViewCellDelegate <NSObject>

- (void)Click:(int)row;
@end

@interface SkTableViewCell : SKNode
@property (nonatomic, assign) id <SkTableViewCellDelegate> delegate;
@property int linha;
@property (strong) NSString* texto;
@property CGRect retangulo;

- (instancetype) initWithNome:(NSString*)Nome andLinha:(int)Linha andRect:(CGRect)rect;
@end
