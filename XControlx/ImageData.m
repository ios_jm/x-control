//
//  ImageData.m
//  JogoSimples
//
//  Created by Tiago Paluch on 14/09/14.
//  Copyright (c) 2014 Rafael Oliveira. All rights reserved.
//

#import "ImageData.h"

@implementation ImageData
- (instancetype)init
{
    self = [super init];
    if (self) {
        _img  = nil;
        _retangulo = CGRectMake(0, 0, 5, 5);
        _status = 0;
    }
    return self;
}
- (void) encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_img forKey:@"Imagem"];
    [encoder encodeFloat:_retangulo.size.height forKey:@"retSh"];
    [encoder encodeFloat:_retangulo.size.width forKey:@"retSw"];
    [encoder encodeFloat:_retangulo.origin.x forKey:@"retOx"];
    [encoder encodeFloat:_retangulo.origin.y forKey:@"retOy"];
    [encoder encodeInt:_status forKey:@"status"];
    
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self) {
        float h, w, x, y;
        _img = [decoder decodeObjectForKey:@"Imagem"];
        h = [decoder decodeFloatForKey:@"retSh"];
        w = [decoder decodeFloatForKey:@"retSw"];
        x = [decoder decodeFloatForKey:@"retOx"];
        y = [decoder decodeFloatForKey:@"retOy"];
        _retangulo = CGRectMake(x, y, w, h);
        _status = [decoder decodeIntForKey:@"status"];
        NSData* imageData = [decoder decodeObjectForKey:@"image"];
        _image = [UIImage imageWithData:imageData];
    }
    return self;
}

- (void) SetImagem:(NSString *)img  {
    [self setImg:img];
    _status++;
}

- (id)copyWithZone:(NSZone *)zone {
    ImageData* im = [[ImageData allocWithZone:zone] init];
    
    [im setImage:_image];
    [im setImg:_img];
    [im setRetangulo:_retangulo];
    
    
    return im;
}
@end
