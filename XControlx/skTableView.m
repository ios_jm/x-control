//
//  skTableView.m
//  XControlx
//
//  Created by Tiago Paluch on 20/09/14.
//  Copyright (c) 2014 Tiago Paluch. All rights reserved.
//

#import "skTableView.h"

@implementation skTableView
{
    SKSpriteNode *fundo;
}
- (void)setConfig:(bool)status {
    
}
- (void)Conection:(bool)status {
    [self UpdateCels];
}
- (void)Click:(int)row {
    [[self servidor] setConectado:YES];
    [[self servidor] conectar:row];
    
    //[skView presentScene:scene];
    [[self servidor] setConfig_status:0];
    GameScene *scene = [[GameScene alloc] initWithSize:[self size]];
    scene.scaleMode = SKSceneScaleModeAspectFit;
    [scene setServidor:[self servidor]];
    [[self servidor] setDelegateCena:scene];
    [[self view] presentScene:scene];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    for (SKNode* obj in [self children]) {
        [obj touchesBegan:touches withEvent:event];
    }
}
- (void)UpdateCels {
    [self removeAllChildren];
    
    fundo = [[SKSpriteNode alloc] initWithImageNamed:@"OmegaPad"];
    fundo.position = CGPointMake(self.size.width/2, self.size.height/2);
    fundo.size = CGSizeMake(fundo.size.width/2, fundo.size.height/2);
    fundo.zPosition = 0;
    [self addChild:fundo];
    
    if ([[[self servidor] services] count]>0) {
        for (int x = 0; x < [[[self servidor] services] count]; x++) {
            
            NSString *str = [[[_servidor services] objectAtIndex:x] name];
            SkTableViewCell* cell = [[SkTableViewCell alloc] initWithNome:str andLinha:x andRect:CGRectMake(0, 0, 300, 100)];
            cell.position = CGPointMake(0, x * 110);
            cell.zPosition = 10;
            [[[self servidor] joyF] setStatus:0];
            [cell setDelegate:self];
            [self addChild:cell];
        }
    } else {
        NSString *str = @"no games nearby";
        SkTableViewCell* cell = [[SkTableViewCell alloc] initWithNome:str andLinha:0 andRect:CGRectMake(0, 0, 300, 100)];
        cell.position = CGPointMake(0, 110);
        cell.zPosition = 10;
        [self addChild:cell];
    }
}
-(void)didMoveToView:(SKView *)view {
    [self UpdateCels];
}
@end
