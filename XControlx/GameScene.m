//
//  GameScene.m
//  XControlx
//
//  Created by Tiago Paluch on 18/09/14.
//  Copyright (c) 2014 Tiago Paluch. All rights reserved.
//

#import "GameScene.h"
#import "skTableView.h"

@implementation GameScene{
    //giroscopio;
    CGFloat acc_x, acc_y, acc_z, rot_x, rot_y, rot_z;
    CGFloat mag, tre, lat, log;
    //Dpad
    CGFloat dpAngle;
    CGPoint dpVel;
    
    BOOL botA, botB, botC;
    SKLabelNode *_sc;
    SKSpriteNode *_compass;
    //SKSpriteNode *fundo;
    
    SKSpriteNode* spritef;
    SKShapeNode* back;
}


-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    _fundo = [[SKNode alloc] init];
    _fundo.position = CGPointMake(0, 0);
    _fundo.zPosition = 0;
    [self addChild:_fundo];
    

    
    //DPad
    _gpad = [[DPad alloc]initWithRect:CGRectMake(0, 0, 96.0f, 96.0f) andColorFill:[UIColor colorWithRed:1 green:0 blue:0 alpha:0.5] andColorStroke:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.8]];
    [_gpad iniciar];
    _gpad.position = CGPointMake(CGRectGetMinX(self.frame)+50, CGRectGetMinY(self.frame)+50);
    _gpad.numberOfDirections = 64;
    _gpad.deadRadius = 32.0f;
    _gpad.zPosition = 10;
    [[self scene] addChild:_gpad];
    //Giroscopio 1
    self.motionManager = [[CMMotionManager alloc] init];
    self.motionManager.accelerometerUpdateInterval = .2;
    self.motionManager.gyroUpdateInterval = .2;
    self.motionManager.deviceMotionUpdateInterval = 5.0 / 60.0;
    [self.motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue]
                                             withHandler:^(CMAccelerometerData  *accelerometerData, NSError *error) {
                                                 [self outputAccelertionData:accelerometerData.acceleration];
                                                 if(error){
                                                     
                                                     NSLog(@"%@", error);
                                                 }
                                             }];
    
    [self.motionManager startGyroUpdatesToQueue:[NSOperationQueue currentQueue]
                                    withHandler:^(CMGyroData *gyroData, NSError *error) {
                                        [self outputRotationData:gyroData.rotationRate];
                                    }];
    [self.motionManager startDeviceMotionUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:^(CMDeviceMotion *motion, NSError *error) {
        [self processMotion:motion];
    }];
    
    [self.motionManager startDeviceMotionUpdatesUsingReferenceFrame:CMAttitudeReferenceFrameXArbitraryZVertical
                                                            toQueue:[NSOperationQueue currentQueue]
                                                        withHandler:^(CMDeviceMotion *motion, NSError *error)
    {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            //CGFloat x = motion.gravity.x;
            //CGFloat y = motion.gravity.y;
            //CGFloat z = motion.gravity.z;
            if (fabs(motion.gravity.x)>0.1) {
                [_Gyro setAcc_x:motion.gravity.x];
                acc_x = motion.gravity.x;
            } else {
                [_Gyro setAcc_x:0];
                acc_x = 0;
            }
            if (fabs(motion.gravity.y)>0.1) {
                [_Gyro setAcc_y:motion.gravity.y];
                acc_y = motion.gravity.y;
            } else {
                [_Gyro setAcc_y:0];
                acc_y = 0;
            }
            if (fabs(motion.gravity.z)>0.1) {
                [_Gyro setAcc_z:motion.gravity.z];
                acc_z = motion.gravity.z;
            } else {
                [_Gyro setAcc_z:0];
                acc_z = 0;
            }
            
        }];
    }];
    _compass = [[SKSpriteNode alloc] initWithImageNamed:@"Compass"];
    _compass.name = @"Compass";
    _compass.xScale = 0.1;
    _compass.yScale = 0.1;
    _compass.position = CGPointMake(CGRectGetMinX(self.frame)+500, CGRectGetMaxY(self.frame)-50);
    _compass.zPosition = 10;
    [self addChild:_compass];
    
    
    //Botoes
    _GbotA = [[Dbutton alloc]initWithRect:CGRectMake(0, 0, 60.0f, 60.0f) andColorFill:[UIColor colorWithRed:1 green:0 blue:0 alpha:0.5] andColorStroke:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.8] label:@"A"];
    //_GbotA.position = CGPointMake(CGRectGetMinX(self.frame)+300, CGRectGetMinY(self.frame)+75);
    _GbotA.zPosition = 10;
    [_GbotA iniciar];
    [[self scene] addChild:_GbotA];
    
    _GbotB = [[Dbutton alloc]initWithRect:CGRectMake(0, 0, 60.0f, 60.0f) andColorFill:[UIColor colorWithRed:1 green:0 blue:0 alpha:0.5] andColorStroke:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.8] label:@"B"];
    //_GbotB.position = CGPointMake(CGRectGetMinX(self.frame)+370, CGRectGetMinY(self.frame)+105);
    _GbotB.zPosition = 10;
    [_GbotB iniciar];
    [[self scene] addChild:_GbotB];
    
    _GbotC = [[Dbutton alloc]initWithRect:CGRectMake(0, 0, 60.0f, 60.0f) andColorFill:[UIColor colorWithRed:1 green:0 blue:0 alpha:0.5] andColorStroke:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.8] label:@"C"];
    //_GbotC.position = CGPointMake(CGRectGetMinX(self.frame)+440, CGRectGetMinY(self.frame)+135);
    _GbotC.zPosition = 10;
    [_GbotC iniciar];
    [[self scene] addChild:_GbotC];
    
    _GbotD = [[Dbutton alloc]initWithRect:CGRectMake(0, 0, 60.0f, 60.0f) andColorFill:[UIColor colorWithRed:1 green:0 blue:0 alpha:0.5] andColorStroke:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.8] label:@"D"];
    _GbotD.zPosition = 10;
    [_GbotD iniciar];
    [[self scene] addChild:_GbotD];

    _Gyro = [[Dgyro alloc] initWithRect:CGRectMake(0, 0, 60.0f, 60.0f) cor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.8]];
    _Gyro.zPosition = 10;
    [_Gyro load];
    [[self scene] addChild:_Gyro];
    
//    _servidor = [[GameClientAppDelegate alloc] init];
//    [[self servidor] setDelegateCena:self];
    
    //label 1
    _sc = [SKLabelNode labelNodeWithFontNamed:@"CourierNewPS-BoldItalicMT"];
    _sc.text = @"0";
    _sc.fontSize = 25;
    _sc.position = CGPointMake(CGRectGetMinX(self.frame)+50, CGRectGetMaxY(self.frame)-50);
    _sc.zPosition = 10;
    [_sc setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
    [_sc setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeLeft];
    [self addChild:_sc];
    
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.locationManager startUpdatingHeading];
    [self.locationManager startUpdatingLocation];
}
-(void)processMotion:(CMDeviceMotion*)motion {
    //NSLog(@"Roll: %.2f Pitch: %.2f Yaw: %.2f", motion.attitude.roll, motion.attitude.pitch, motion.attitude.yaw);

}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
}
-(void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
//    self.magneticHeading.text = [NSString stringWithFormat:@"%f", newHeading.magneticHeading];
//    self.trueHeading.text = [NSString stringWithFormat:@"%f", newHeading.trueHeading];
//    self.lonLabel.text = [NSString stringWithFormat:@"%f", self.locationManager.location.coordinate.longitude];
//    self.latLabel.text = [NSString stringWithFormat:@"%f", self.locationManager.location.coordinate.latitude];
    mag = newHeading.magneticHeading;
    tre = newHeading.trueHeading;
    lat = self.locationManager.location.coordinate.latitude;
    log = self.locationManager.location.coordinate.longitude;
    //NSLog(@"Atualiza");
}
-(void)update:(CFTimeInterval)currentTime {
    botA = _GbotA.ativo;
    botB = _GbotB.ativo;
    botC = _GbotC.ativo;
    //botD = _GbotD.ativo;
    dpAngle = _gpad.degrees;
    dpVel = _gpad.velocity;
    
    _compass.zRotation = tre;
    //NSLog(@"%f",tre);
    GameData* mGd = [[GameData alloc] init];
    
    [mGd setBotA:botA];
    [mGd setBotB:botB];
    [mGd setBotC:botC];
    [mGd setBotD:_GbotD.ativo];
    [mGd setDpAngle:dpAngle];
    [mGd setDpVel:dpVel];
    [mGd setMag:mag];
    [mGd setTre:tre];
    [mGd setAcc_x:acc_x];
    [mGd setAcc_y:acc_y];
    [mGd setAcc_z:acc_z];
    
    [mGd setRot_x:rot_x];
    [mGd setRot_y:rot_y];
    [mGd setRot_z:rot_z];
    
    
    
    //[_Gyro setAcc_x:acc_x];
    //[_Gyro setAcc_y:acc_y];
    //[_Gyro setAcc_z:acc_z];
    
    //[_Gyro setRot_x:rot_x];
    //[_Gyro setRot_y:rot_y];
    //[_Gyro setRot_z:rot_z];
    
    [_Gyro update];
    
    [self ComandoGd:mGd];

    
    
    

        [self ajustaTela];

    //if ([self score] != [[[self servidor] joyS] score]) {
        [self ajustaScore];
    //}
}
- (void) ajustaScore {
    
    if ([[[self servidor] JoySqueue] count] > 0) {
        ServerData* qsd = [[[self servidor] JoySqueue] dequeue];
        if ([[_servidor joyS] ver] < [qsd ver]) {
            [[self servidor] setJoyS:qsd];
        }
    }
    ServerData* cdc = [_servidor joyS];
    [self setScore:[cdc score]];
    [_sc setText:[NSString stringWithFormat:@"Score: %d",_score]];
}
- (void) ajustaTela {
    
    if ([[[self servidor] CFGSqueue] count] > 0) {
        ConfigData* cfg = [[[self servidor] CFGSqueue] dequeue];
        if ([[_servidor joyF] status] < [cfg status]) {
            [[self servidor] setJoyF:cfg];
            NSLog(@"aplicando %d",cfg.status);
    
    _compass.hidden = YES;
    
    
    
    NSString* img = @"";
    
    switch (cfg.fundoStylo) {
        case 1:
            img = @"sci-fi";
            break;
        case 2:
            img = @"arcade";
            break;
        case 3:
            img = @"rustico";
            break;
        default:
            img = @"sci-fi";
            break;
    }
    spritef = [[SKSpriteNode alloc] initWithImageNamed:[NSString stringWithFormat:@"%@-back",img]];
    
    spritef.size = self.frame.size;
    spritef.position = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    back = [[SKShapeNode alloc] init];
    [back setFillColor:cfg.Fundo];
    [back setPath:CGPathCreateWithRect( self.frame, nil)];
    
    [[self fundo] removeAllChildren];
    if (cfg.fundoStylo==0) {
        back.hidden = NO;
        spritef.hidden = YES;
    } else {
        back.hidden = YES;
        spritef.hidden = NO;
    }
    [[self fundo] addChild:back];
    [[self fundo] addChild:spritef];
    
    
    [_GbotA setTexto:cfg.btAlabel];
    [_GbotB setTexto:cfg.btBlabel];
    [_GbotC setTexto:cfg.btClabel];
    [_GbotD setTexto:cfg.btDlabel];
    
    
    [[_GbotA botao] setFillColor:cfg.btAColor1];
    [[_GbotA botao] setStrokeColor:cfg.btAColor2];

    [[_GbotB botao] setFillColor:cfg.btBColor1];
    [[_GbotB botao] setStrokeColor:cfg.btBColor2];
    
    [[_GbotC botao] setFillColor:cfg.btCColor1];
    [[_GbotC botao] setStrokeColor:cfg.btCColor2];
    
    [[_GbotD botao] setFillColor:cfg.btDColor1];
    [[_GbotD botao] setStrokeColor:cfg.btDColor2];
    
    
    [_GbotA setStylo:cfg.btAStylo];
    [_GbotB setStylo:cfg.btBStylo];
    [_GbotC setStylo:cfg.btCStylo];
    [_GbotD setStylo:cfg.btDStylo];
    [_Gyro setStylo:cfg.gyroStylo];
    [_Gyro setColor:cfg.dPadColor1];
    [_Gyro setDimencoes:cfg.btGy];
    [_GbotA setRect:cfg.btA];
    [_GbotB setRect:cfg.btB];
    [_GbotC setRect:cfg.btC];
    [_GbotD setRect:cfg.btD];
    [_gpad setRect:cfg.dPad];
    [_gpad setStylo:cfg.dpadStylo];
    
    
    [[self servidor] setConfig_status:[cfg status]];
    

    
    [_GbotA iniciar];
    [_GbotB iniciar];
    [_GbotC iniciar];
    [_GbotD iniciar];
    [_gpad iniciar];
    [_Gyro load];
            
            if (cfg.LetraS == -1) {
                _sc.hidden = YES;
            } else {
                _sc.hidden = NO;
            }
            _sc.position = cfg.LetraP;
            const CGFloat  *lcolor = CGColorGetComponents(cfg.LetraC.CGColor);
            [_sc setFontColor:[SKColor colorWithRed:lcolor[0] green:lcolor[1] blue:lcolor[2] alpha:lcolor[3]]];
            _sc.zPosition = 10;
            //_sc.color = cfg.LetraC;
            //_sc.colorBlendFactor = 1;
            
            [_sc setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeCenter];
            
            if (cfg.LetraA==1) {
                [_sc setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeLeft];
            }
            if (cfg.LetraA==2) {
                [_sc setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeRight];
            }
    
        }
    }

}
#pragma Comando
- (void)Comando:(NSString *)comando {
    NSLog(@"%@",comando);
    //[[self servidor] enviar:[NSString stringWithFormat:@"%@",comando ]];
}
- (void)ComandoGd:(GameData *)gData {
    [[self servidor] enviarGd:gData];
}
#pragma Giroscopio
-(void)outputAccelertionData:(CMAcceleration)acceleration{
//    if (fabs(acceleration.x) > 0.15) {
//         acc_x = acceleration.x;
//    } else {
//         acc_x = 0;
//    }
//    if (fabs(acceleration.y) > 0.15) {
//        acc_y = acceleration.y;
//    } else {
//        acc_y = 0;
//    }
//    if (fabs(acceleration.z) > 0.15) {
//        acc_z = acceleration.z;
//    } else {
//        acc_z = 0;
//    }
}
-(void)outputRotationData:(CMRotationRate)rotation{
    if (fabs(rotation.x) > 0.15) {
        rot_x = rotation.x;
    } else {
        rot_x = 0;
    }
    if (fabs(rotation.y) > 0.15) {
        rot_y = rotation.y;
    } else {
        rot_y = 0;
    }
    if (fabs(rotation.z) > 0.15) {
        rot_z = rotation.z;
    } else {
        rot_z = 0;
    }
}
-(void)setConfig:(bool)status {
    
    if (status) {
        ImageData *sv = [_servidor JoyI];
        SKTexture *tx = [SKTexture textureWithImage:[sv image]];
        SKSpriteNode* temp = [[SKSpriteNode alloc] initWithTexture:tx];
        [temp setColor:[UIColor colorWithRed:1.0f green:1.0f blue:0.0f alpha:1.0f]];
        [temp setSize:CGSizeMake(sv.retangulo.size.width, sv.retangulo.size.height)];
        [temp setPosition:CGPointMake(sv.retangulo.origin.x, sv.retangulo.origin.y)];
        [[self fundo] addChild:temp];
    }
}
- (void)Conection:(bool)status {
    if (!status) {
        [self.motionManager stopAccelerometerUpdates];
        [self.motionManager stopDeviceMotionUpdates];
        [self.motionManager stopGyroUpdates];
        [self.motionManager stopMagnetometerUpdates];
        skTableView* stv = [[skTableView alloc] initWithSize:[self size]];
        stv.scaleMode = SKSceneScaleModeAspectFit;
        [stv setServidor:[self servidor]];
        [[self servidor] setDelegate:stv];
        [[self servidor] load];
        [[self view] presentScene:stv];
    }
}

@end
