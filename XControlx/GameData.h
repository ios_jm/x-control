//
//  GameData.h
//  ServidorControle
//
//  Created by Tiago Paluch on 24/07/14.
//  Copyright (c) 2014 Tiago Paluch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>

@interface GameData : NSObject <NSCopying>
@property float dpAngle;
@property CGPoint dpVel;


@property float acc_x;
@property float acc_y;
@property float acc_z;
@property float rot_x;
@property float rot_y;
@property float rot_z;

@property float mag;
@property float tre;
@property float lat;
@property float log;

@property BOOL botA;
@property BOOL botB;
@property BOOL botC;
@property BOOL botD;

@property int Config;
@property (strong) NSString* chat;
- (id)copyWithZone:(NSZone *)zone;
@end
