//
//  GameScene.h
//  XControlx
//

//  Copyright (c) 2014 Tiago Paluch. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <CoreMotion/CoreMotion.h>
#import <CoreLocation/CoreLocation.h>
#import "DPad.h"
#import "Dbutton.h"
#import "Dgyro.h"
#import "GameClientAppDelegate.h"
#import "GameData.h"

@interface GameScene : SKScene <GamejoyDelegate,CLLocationManagerDelegate>
@property (strong, nonatomic) CMMotionManager *motionManager;
@property (strong) GameClientAppDelegate* servidor;
@property (strong) DPad* gpad;
@property (strong) Dbutton* GbotA;
@property (strong) Dbutton* GbotB;
@property (strong) Dbutton* GbotC;
@property (strong) Dbutton* GbotD;
@property (strong) Dgyro* Gyro;
@property (strong) SKNode* fundo;
@property (strong, nonatomic) CLLocationManager *locationManager;

@property int score;
//envia comando
- (void) Comando:(NSString*)comando;
- (void) ComandoGd:(GameData*)gData;
- (void) Conection:(bool)status;
- (void) setConfig:(bool)status;

@end
