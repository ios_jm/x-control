//
//  TableViewController.h
//  XControlx
//
//  Created by Tiago Paluch on 20/09/14.
//  Copyright (c) 2014 Tiago Paluch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameClientAppDelegate.h"
@interface TableViewController : UITableViewController
@property (strong) GameClientAppDelegate* servidor;
- (void) Conection:(bool)status;
- (void) setConfig:(bool)status;


@end
