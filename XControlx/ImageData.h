//
//  ImageData.h
//  JogoSimples
//
//  Created by Tiago Paluch on 14/09/14.
//  Copyright (c) 2014 Rafael Oliveira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>

@interface ImageData : NSObject <NSCopying>
@property CGRect retangulo;
@property NSString* img;
@property int status;

@property (strong) UIImage* image;
- (void) SetImagem:(NSString *)img;
- (id)copyWithZone:(NSZone *)zone;
@end
