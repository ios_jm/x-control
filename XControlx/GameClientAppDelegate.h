

#import <CFNetwork/CFNetwork.h>
#import "GameData.h"
#import "ServerData.h"
#import "ConfigData.h"
#import "ImageData.h"
#import "GZIP.h"
#import "NSMutableArray+QueueStack.h"

@class GameClientAppDelegate;
@protocol GamejoyDelegate <NSObject>

- (void)Conection:(bool)status;
- (void)setConfig:(bool)status;
@end


@interface GameClientAppDelegate : NSObject
@property (nonatomic, assign) id <GamejoyDelegate> delegate;
@property (nonatomic, assign) id <GamejoyDelegate> delegateCena;
@property (nonatomic, strong, readwrite) NSMutableArray* services; // of NSNetService
@property (strong) NSMutableData* receivedData;
@property BOOL conectado;
@property (strong) HsuQueue* JoySqueue;
@property (strong) HsuQueue* CFGSqueue;
@property (strong) ServerData* joyS;
@property (strong) ConfigData* joyF;
@property (strong) ImageData* JoyI;
@property (strong) NSString* url;
@property int config_status;

- (void) load;
- (void) conectar:(int)index;
- (void) enviarGd:(GameData*)gData;
@end
