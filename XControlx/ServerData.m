//
//  ServerData.m
//  JogoSimples
//
//  Created by Tiago Paluch on 21/08/14.
//  Copyright (c) 2014 Rafael Oliveira. All rights reserved.
//

#import "ServerData.h"

@implementation ServerData
- (instancetype)init
{
    self = [super init];
    if (self) {
        _texto = @" ";
        _mId = @" ";
        _ver = 0;
        _score = 0;
    }
    return self;
}
- (void) encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_texto forKey:@"Texto"];
    [encoder encodeObject:_mId forKey:@"mId"];
    [encoder encodeInteger:_ver forKey:@"ver"];
    [encoder encodeInteger:_score forKey:@"score"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self) {
        _texto = [decoder decodeObjectForKey:@"Texto"];
        _mId = [decoder decodeObjectForKey:@"mId"];
        _ver = (int)[decoder decodeIntegerForKey:@"ver"];
        _score = (int)[decoder decodeIntegerForKey:@"score"];
    }
    return self;
}
- (id)copyWithZone:(NSZone *)zone {
    ServerData* sd = [[ServerData allocWithZone:zone] init];
    
    [sd setMId:_mId];
    [sd setTexto:_texto];
    [sd setVer:_ver];
    [sd setScore:_score];

    return sd;
}
- (void)Scor:(int)_sc {
    if (_sc != _score) {
        [self setScore:_sc];
        self.ver++;
    }
}
- (void)Text:(NSString *)_tx {
    if ([_tx caseInsensitiveCompare:_texto]!=NSOrderedSame) {
        [self setTexto:_tx];
        self.ver++;
    }
}
@end
