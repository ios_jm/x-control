//
//  ServerData.h
//  JogoSimples
//
//  Created by Tiago Paluch on 21/08/14.
//  Copyright (c) 2014 Rafael Oliveira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>

@interface ServerData : NSObject <NSCopying>
@property (strong) NSString* texto;
@property (strong) NSString* mId;
@property int ver;
@property int score;
-(id) copyWithZone: (NSZone *) zone;

- (void)Scor:(int)_sc;
- (void)Text:(NSString*)_tx;
@end
