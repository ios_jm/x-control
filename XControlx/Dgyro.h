//
//  Dgyro.h
//  XControlx
//
//  Created by Tiago Paluch on 01/11/14.
//  Copyright (c) 2014 Tiago Paluch. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Dgyro : SKNode
@property float acc_x;
@property float acc_y;
@property float acc_z;
@property float rot_x;
@property float rot_y;
@property float rot_z;

@property CGRect dimencoes;
@property UIColor* color;
@property CGPoint mcelu;
@property (nonatomic, assign) CGRect framepai;
@property int Stylo;

- (instancetype) initWithRect:(CGRect)rect cor:(UIColor*)cor;
- (void) load;
- (void) update;
- (void) redraw;
@end
