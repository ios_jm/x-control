//
//  Dgyro.m
//  XControlx
//
//  Created by Tiago Paluch on 01/11/14.
//  Copyright (c) 2014 Tiago Paluch. All rights reserved.
//

#import "Dgyro.h"

@implementation Dgyro
{

    SKShapeNode *base;
    SKShapeNode *centro;
    SKShapeNode *celular;
    SKSpriteNode* bt1;
    SKSpriteNode* bt2;
    BOOL first;
}

- (instancetype)initWithRect:(CGRect)rect cor:(UIColor*)cor {
    self = [super init];
    if (self) {
        _color = cor;
        _dimencoes = rect;
        _acc_x=0;
        _acc_y=0;
        _acc_z=0;
        _rot_x=0;
        _rot_y=0;
        _rot_z=0;
        first = YES;
        
    }
    return self;
}
- (void)load {
    if (!first ) {
        [self removeAllChildren];
    }
    first = NO;
    
    if (_Stylo==-1) {
        self.hidden=YES;
    } else {
        self.hidden=NO;
    }
    
    CGPoint center = _dimencoes.origin;
    center.x += _dimencoes.size.width/2;
    center.y += _dimencoes.size.height/2;
    
    CGRect novo;
    
    novo.size.width = _dimencoes.size.width / 5;
    novo.size.height = _dimencoes.size.height / 5;
    novo.origin.x = center.x - (novo.size.width / 2);
    novo.origin.y = center.y - (novo.size.height / 2);
    
    base = [[SKShapeNode alloc] init];
    base.fillColor = nil;
    base.strokeColor = _color;
    base.lineWidth = 1.0f;
    base.path = CGPathCreateWithEllipseInRect(_dimencoes, NULL);
    base.alpha = 1.0f;
    
    [self addChild:base];
    
    centro = [[SKShapeNode alloc] init];
    centro.fillColor = _color;
    centro.strokeColor = _color;
    centro.lineWidth = 1.0f;
    centro.path = CGPathCreateWithEllipseInRect(novo, NULL);
    centro.alpha = 0.2f;
    
    [self addChild:centro];
    
    _mcelu = center;
    celular = [[SKShapeNode alloc] init];
    celular.fillColor = _color;
    celular.strokeColor = _color;
    celular.lineWidth = 1.0f;
    celular.path = CGPathCreateWithEllipseInRect(novo, NULL);
    celular.alpha = 1.0f;
    
    [self addChild:celular];
    
    NSString* img = @"";
    
    switch (_Stylo) {
        case 1:
            img = @"sci-fi";
            break;
        case 2:
            img = @"arcade";
            break;
        case 3:
            img = @"rustico";
            break;
        default:
            img = @"sci-fi";
            break;
    }
    bt1 = [[SKSpriteNode alloc] initWithImageNamed:[NSString stringWithFormat:@"%@-giroscopio-03A",img]];
    bt2 = [[SKSpriteNode alloc] initWithImageNamed:[NSString stringWithFormat:@"%@-giroscopio-bb",img]];
    
    bt1.position = center;
    bt2.position = center;
    bt1.size = _dimencoes.size;
    bt2.size = novo.size;
    
    [self addChild:bt1];
    [self addChild:bt2];
    
    if (_Stylo==0) {
        bt1.hidden = YES;
        bt2.hidden = YES;
        base.hidden = NO;
        centro.hidden = NO;
        celular.hidden = NO;
    } else {
        bt1.hidden = NO;
        bt2.hidden = NO;
        base.hidden = YES;
        centro.hidden = YES;
        celular.hidden = YES;
    }
}
- (void)update {
    _mcelu.x = (-_acc_y + 1.0) * (_dimencoes.size.width / 2);
    _mcelu.y = (_acc_x + 1.0) * (_dimencoes.size.height / 2);
    

    CGPoint center = _dimencoes.origin;
    
    CGRect novo;
    
    novo.size.width = _dimencoes.size.width / 5;
    novo.size.height = _dimencoes.size.height / 5;
    novo.origin.x = _mcelu.x - (novo.size.width / 2)+center.x;
    novo.origin.y = _mcelu.y - (novo.size.height / 2)+center.y;
    
    bt2.position = CGPointMake(_mcelu.x+center.x, _mcelu.y+center.y);
    //aqui ta o erro
    celular.path = CGPathCreateWithEllipseInRect(novo, NULL);
    
}
- (void)redraw {
    
}
@end
