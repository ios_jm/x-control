//
//  main.m
//  XControlx
//
//  Created by Tiago Paluch on 18/09/14.
//  Copyright (c) 2014 Tiago Paluch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
