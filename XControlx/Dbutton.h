//
//  Dbutton.h
//  Joystick
//
//  Created by Tiago Paluch on 24/07/14.
//  Copyright (c) 2014 Tiago Paluch. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Dbutton : SKNode

@property int estilo;

@property (nonatomic, strong) SKShapeNode* botao;
@property (nonatomic, assign) BOOL ativo;
@property (nonatomic, assign) CGFloat bRadius;
@property (nonatomic, strong) SKLabelNode *title;
@property (nonatomic, strong) NSString* texto;
@property (nonatomic, assign) CGRect rect;
@property (strong) UIColor* corF;
@property (strong) UIColor* corS;
@property (nonatomic, assign) CGRect framepai;

@property int Stylo;
- (instancetype) initWithRect:(CGRect)rect andColorFill:(UIColor *)colorF andColorStroke:(UIColor *)colorS;

- (instancetype) initWithRect:(CGRect)rect andColorFill:(UIColor *)colorF andColorStroke:(UIColor *)colorS label:(NSString*)blabel;

- (instancetype) initWithRect:(CGRect)rect andStylo:(int) sty andLabel:(NSString*)blabel;

- (void) iniciar;
@end
