//
//  ConfigData.m
//  xControler
//
//  Created by Tiago Paluch on 11/09/14.
//  Copyright (c) 2014 Tiago Paluch. All rights reserved.
//

#import "ConfigData.h"

@implementation ConfigData
- (instancetype)init
{
    self = [super init];
    if (self) {
        _mId = @" ";
        _status = 0;
        _Fundo = [UIColor colorWithRed:0 green:0 blue:0 alpha:1];
        _btAColor1 = [UIColor colorWithRed:1 green:0 blue:0 alpha:0.5];
        _btAColor2 = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
        _btBColor1 = [UIColor colorWithRed:1 green:0 blue:0 alpha:0.5];
        _btBColor2 = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
        _btCColor1 = [UIColor colorWithRed:1 green:0 blue:0 alpha:0.5];
        _btCColor2 = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
        _btDColor1 = [UIColor colorWithRed:1 green:0 blue:0 alpha:0.5];
        _btDColor2 = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
        _dPadColor1 = [UIColor colorWithRed:1 green:0 blue:0 alpha:0.5];
        _dPadColor2 = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
        
        _btAlabel = @"A";
        _btBlabel = @"B";
        _btClabel = @"C";
        _btDlabel = @"D";
        
        _btAHorizontalAlign = 0;
        _btAVerticalAlign = 0;
        _btBHorizontalAlign = 0;
        _btBVerticalAlign = 0;
        _btCHorizontalAlign = 0;
        _btCVerticalAlign = 0;
        _btDHorizontalAlign = 0;
        _btDVerticalAlign = 0;
        
        _ImgNode = nil;
        _ImgRect = CGRectMake(0, 0, 96, 96);
        
        _dPad = CGRectMake(50, 50, 60, 60);
        _btA = CGRectMake(300, 75, 60, 60);
        _btB = CGRectMake(370, 105, 60, 60);
        _btC = CGRectMake(440, 135, 60, 60);
        _btD = CGRectMake(370, 200, 60, 60);
        _btGy = CGRectMake(0, 0, 100, 100);
        
        _btAStylo=0;
        _btBStylo=0;
        _btCStylo=0;
        _btDStylo=0;
        _dpadStylo=0;
        _gyroStylo=0;
        _fundoStylo=0;
        
        _LetraA = 0;
        _LetraS = 0;
        _LetraC = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
        _LetraP = CGPointMake(50, 300);
        
    }
    return self;
}
- (void) encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_mId forKey:@"mId"];
    [encoder encodeInt:_status forKey:@"status"];
    
    [encoder encodeInt:_btAVerticalAlign forKey:@"btAVerticalAlign"];
    [encoder encodeInt:_btAHorizontalAlign forKey:@"btAHorizontalAlign"];
    [encoder encodeInt:_btBVerticalAlign forKey:@"btBVerticalAlign"];
    [encoder encodeInt:_btBHorizontalAlign forKey:@"btBHorizontalAlign"];
    [encoder encodeInt:_btCVerticalAlign forKey:@"btCVerticalAlign"];
    [encoder encodeInt:_btCHorizontalAlign forKey:@"btCHorizontalAlign"];
    [encoder encodeInt:_btDVerticalAlign forKey:@"btDVerticalAlign"];
    [encoder encodeInt:_btDHorizontalAlign forKey:@"btDHorizontalAlign"];
    
    [encoder encodeObject:_btAlabel forKey:@"btAlabel"];
    [encoder encodeObject:_btBlabel forKey:@"btBlabel"];
    [encoder encodeObject:_btClabel forKey:@"btClabel"];
    [encoder encodeObject:_btDlabel forKey:@"btDlabel"];
    [encoder encodeObject:_ImgNode forKey:@"ImgNode"];
    
    [encoder encodeFloat:_dPad.origin.x forKey:@"dpadx"];
    [encoder encodeFloat:_dPad.origin.y forKey:@"dpady"];
    [encoder encodeFloat:_dPad.size.width forKey:@"dpadw"];
    [encoder encodeFloat:_dPad.size.height forKey:@"dpadh"];
    
    [encoder encodeFloat:_btA.origin.x forKey:@"btax"];
    [encoder encodeFloat:_btA.origin.y forKey:@"btay"];
    [encoder encodeFloat:_btA.size.width forKey:@"btaw"];
    [encoder encodeFloat:_btA.size.height forKey:@"btah"];
    
    [encoder encodeFloat:_btB.origin.x forKey:@"btbx"];
    [encoder encodeFloat:_btB.origin.y forKey:@"btby"];
    [encoder encodeFloat:_btB.size.width forKey:@"btbw"];
    [encoder encodeFloat:_btB.size.height forKey:@"btbh"];
    
    [encoder encodeFloat:_btC.origin.x forKey:@"btcx"];
    [encoder encodeFloat:_btC.origin.y forKey:@"btcy"];
    [encoder encodeFloat:_btC.size.width forKey:@"btcw"];
    [encoder encodeFloat:_btC.size.height forKey:@"btch"];
    
    [encoder encodeFloat:_btD.origin.x forKey:@"btdx"];
    [encoder encodeFloat:_btD.origin.y forKey:@"btdy"];
    [encoder encodeFloat:_btD.size.width forKey:@"btdw"];
    [encoder encodeFloat:_btD.size.height forKey:@"btdh"];

    [encoder encodeFloat:_btGy.origin.x forKey:@"btgx"];
    [encoder encodeFloat:_btGy.origin.y forKey:@"btgy"];
    [encoder encodeFloat:_btGy.size.width forKey:@"btgw"];
    [encoder encodeFloat:_btGy.size.height forKey:@"btgh"];
    
    [encoder encodeFloat:_ImgRect.origin.x forKey:@"imx"];
    [encoder encodeFloat:_ImgRect.origin.y forKey:@"imy"];
    [encoder encodeFloat:_ImgRect.size.width forKey:@"imw"];
    [encoder encodeFloat:_ImgRect.size.height forKey:@"imh"];
    
    const CGFloat  *fundo_components = CGColorGetComponents(_Fundo.CGColor);
    [encoder encodeFloat:fundo_components[0] forKey:@"fundo_r"];
    [encoder encodeFloat:fundo_components[1] forKey:@"fundo_g"];
    [encoder encodeFloat:fundo_components[2] forKey:@"fundo_b"];
    [encoder encodeFloat:fundo_components[3] forKey:@"fundo_a"];
    
    const CGFloat  *dpc1_components = CGColorGetComponents(_dPadColor1.CGColor);
    [encoder encodeFloat:dpc1_components[0] forKey:@"pd1_r"];
    [encoder encodeFloat:dpc1_components[1] forKey:@"pd1_g"];
    [encoder encodeFloat:dpc1_components[2] forKey:@"pd1_b"];
    [encoder encodeFloat:dpc1_components[3] forKey:@"pd1_a"];
    
    const CGFloat  *dpc2_components = CGColorGetComponents(_dPadColor2.CGColor);
    [encoder encodeFloat:dpc2_components[0] forKey:@"pd2_r"];
    [encoder encodeFloat:dpc2_components[1] forKey:@"pd2_g"];
    [encoder encodeFloat:dpc2_components[2] forKey:@"pd2_b"];
    [encoder encodeFloat:dpc2_components[3] forKey:@"pd2_a"];
    
    const CGFloat  *ba1_components = CGColorGetComponents(_btAColor1.CGColor);
    [encoder encodeFloat:ba1_components[0] forKey:@"a1_r"];
    [encoder encodeFloat:ba1_components[1] forKey:@"a1_g"];
    [encoder encodeFloat:ba1_components[2] forKey:@"a1_b"];
    [encoder encodeFloat:ba1_components[3] forKey:@"a1_a"];
    
    const CGFloat  *ba2_components = CGColorGetComponents(_btAColor2.CGColor);
    [encoder encodeFloat:ba2_components[0] forKey:@"a2_r"];
    [encoder encodeFloat:ba2_components[1] forKey:@"a2_g"];
    [encoder encodeFloat:ba2_components[2] forKey:@"a2_b"];
    [encoder encodeFloat:ba2_components[3] forKey:@"a2_a"];
    
    const CGFloat  *bb1_components = CGColorGetComponents(_btBColor1.CGColor);
    [encoder encodeFloat:bb1_components[0] forKey:@"b1_r"];
    [encoder encodeFloat:bb1_components[1] forKey:@"b1_g"];
    [encoder encodeFloat:bb1_components[2] forKey:@"b1_b"];
    [encoder encodeFloat:bb1_components[3] forKey:@"b1_a"];
    
    const CGFloat  *bb2_components = CGColorGetComponents(_btBColor2.CGColor);
    [encoder encodeFloat:bb2_components[0] forKey:@"b2_r"];
    [encoder encodeFloat:bb2_components[1] forKey:@"b2_g"];
    [encoder encodeFloat:bb2_components[2] forKey:@"b2_b"];
    [encoder encodeFloat:bb2_components[3] forKey:@"b2_a"];
    
    const CGFloat  *bc1_components = CGColorGetComponents(_btCColor1.CGColor);
    [encoder encodeFloat:bc1_components[0] forKey:@"c1_r"];
    [encoder encodeFloat:bc1_components[1] forKey:@"c1_g"];
    [encoder encodeFloat:bc1_components[2] forKey:@"c1_b"];
    [encoder encodeFloat:bc1_components[3] forKey:@"c1_a"];
    
    const CGFloat  *bc2_components = CGColorGetComponents(_btCColor2.CGColor);
    [encoder encodeFloat:bc2_components[0] forKey:@"c2_r"];
    [encoder encodeFloat:bc2_components[1] forKey:@"c2_g"];
    [encoder encodeFloat:bc2_components[2] forKey:@"c2_b"];
    [encoder encodeFloat:bc2_components[3] forKey:@"c2_a"];
    
    const CGFloat  *bd1_components = CGColorGetComponents(_btDColor1.CGColor);
    [encoder encodeFloat:bd1_components[0] forKey:@"d1_r"];
    [encoder encodeFloat:bd1_components[1] forKey:@"d1_g"];
    [encoder encodeFloat:bd1_components[2] forKey:@"d1_b"];
    [encoder encodeFloat:bd1_components[3] forKey:@"d1_a"];
    
    const CGFloat  *bd2_components = CGColorGetComponents(_btDColor2.CGColor);
    [encoder encodeFloat:bd2_components[0] forKey:@"d2_r"];
    [encoder encodeFloat:bd2_components[1] forKey:@"d2_g"];
    [encoder encodeFloat:bd2_components[2] forKey:@"d2_b"];
    [encoder encodeFloat:bd2_components[3] forKey:@"d2_a"];
    
    
    
    [encoder encodeInt:_btAStylo forKey:@"btAStylo"];
    [encoder encodeInt:_btBStylo forKey:@"btBStylo"];
    [encoder encodeInt:_btCStylo forKey:@"btCStylo"];
    [encoder encodeInt:_btDStylo forKey:@"btDStylo"];
    [encoder encodeInt:_dpadStylo forKey:@"dpadStylo"];
    [encoder encodeInt:_gyroStylo forKey:@"gyroStylo"];
    [encoder encodeInt:_fundoStylo forKey:@"fundoStylo"];
    
    [encoder encodeInt:_LetraA forKey:@"LetraA"];
    [encoder encodeInt:_LetraS forKey:@"LetraS"];
    [encoder encodeFloat:_LetraP.x forKey:@"lpx"];
    [encoder encodeFloat:_LetraP.y forKey:@"lpy"];
    const CGFloat  *letraCcomponentes = CGColorGetComponents(_LetraC.CGColor);
    [encoder encodeFloat:letraCcomponentes[0] forKey:@"lc_r"];
    [encoder encodeFloat:letraCcomponentes[1] forKey:@"lc_g"];
    [encoder encodeFloat:letraCcomponentes[2] forKey:@"lc_b"];
    [encoder encodeFloat:letraCcomponentes[3] forKey:@"lc_a"];
    
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self) {
        _mId = [decoder decodeObjectForKey:@"mId"];
        _status = [decoder decodeIntForKey:@"status"];
        
        
        _btAVerticalAlign = [decoder decodeIntForKey:@"btAVerticalAlign"];
        _btAHorizontalAlign = [decoder decodeIntForKey:@"btAHorizontalAlign"];
        _btBVerticalAlign = [decoder decodeIntForKey:@"btBVerticalAlign"];
        _btBHorizontalAlign = [decoder decodeIntForKey:@"btBHorizontalAlign"];
        _btCVerticalAlign = [decoder decodeIntForKey:@"btCVerticalAlign"];
        _btCHorizontalAlign = [decoder decodeIntForKey:@"btCHorizontalAlign"];
        _btDVerticalAlign = [decoder decodeIntForKey:@"btDVerticalAlign"];
        _btDHorizontalAlign = [decoder decodeIntForKey:@"btDHorizontalAlign"];
        
        _btAlabel = [decoder decodeObjectForKey:@"btAlabel"];
        _btBlabel = [decoder decodeObjectForKey:@"btBlabel"];
        _btClabel = [decoder decodeObjectForKey:@"btClabel"];
        _btDlabel = [decoder decodeObjectForKey:@"btDlabel"];
        _ImgNode = [decoder decodeObjectForKey:@"ImgNode"];
        
        
        
        _dPad = CGRectMake([decoder decodeFloatForKey:@"dpadx"],
                           [decoder decodeFloatForKey:@"dpady"],
                           [decoder decodeFloatForKey:@"dpadw"],
                           [decoder decodeFloatForKey:@"dpadh"]);
 
        _btA = CGRectMake([decoder decodeFloatForKey:@"btax"],
                          [decoder decodeFloatForKey:@"btay"],
                          [decoder decodeFloatForKey:@"btaw"],
                          [decoder decodeFloatForKey:@"btah"]);
        _btB = CGRectMake([decoder decodeFloatForKey:@"btbx"],
                          [decoder decodeFloatForKey:@"btby"],
                          [decoder decodeFloatForKey:@"btbw"],
                          [decoder decodeFloatForKey:@"btbh"]);
        _btC = CGRectMake([decoder decodeFloatForKey:@"btcx"],
                          [decoder decodeFloatForKey:@"btcy"],
                          [decoder decodeFloatForKey:@"btcw"],
                          [decoder decodeFloatForKey:@"btch"]);
        _btD = CGRectMake([decoder decodeFloatForKey:@"btdx"],
                          [decoder decodeFloatForKey:@"btdy"],
                          [decoder decodeFloatForKey:@"btdw"],
                          [decoder decodeFloatForKey:@"btdh"]);
        _btGy = CGRectMake([decoder decodeFloatForKey:@"btgx"],
                          [decoder decodeFloatForKey:@"btgy"],
                          [decoder decodeFloatForKey:@"btgw"],
                          [decoder decodeFloatForKey:@"btgh"]);
        _ImgRect = CGRectMake([decoder decodeFloatForKey:@"imx"],
                              [decoder decodeFloatForKey:@"imy"],
                              [decoder decodeFloatForKey:@"imw"],
                              [decoder decodeFloatForKey:@"imh"]);
        
        
        _Fundo = [UIColor colorWithRed:[decoder decodeFloatForKey:@"fundo_r"]
                                 green:[decoder decodeFloatForKey:@"fundo_g"]
                                  blue:[decoder decodeFloatForKey:@"fundo_b"]
                                 alpha:[decoder decodeFloatForKey:@"fundo_a"]];
        
        
        _dPadColor1 = [UIColor colorWithRed:[decoder decodeFloatForKey:@"pd1_r"]
                                     green:[decoder decodeFloatForKey:@"pd1_g"]
                                      blue:[decoder decodeFloatForKey:@"pd1_b"]
                                     alpha:[decoder decodeFloatForKey:@"pd1_a"]];
        _dPadColor2 = [UIColor colorWithRed:[decoder decodeFloatForKey:@"pd2_r"]
                                      green:[decoder decodeFloatForKey:@"pd2_g"]
                                       blue:[decoder decodeFloatForKey:@"pd2_b"]
                                      alpha:[decoder decodeFloatForKey:@"pd2_a"]];
        _btAColor1 = [UIColor colorWithRed:[decoder decodeFloatForKey:@"a1_r"]
                                     green:[decoder decodeFloatForKey:@"a1_g"]
                                      blue:[decoder decodeFloatForKey:@"a1_b"]
                                     alpha:[decoder decodeFloatForKey:@"a1_a"]];
        _btAColor2 = [UIColor colorWithRed:[decoder decodeFloatForKey:@"a2_r"]
                                     green:[decoder decodeFloatForKey:@"a2_g"]
                                      blue:[decoder decodeFloatForKey:@"a2_b"]
                                     alpha:[decoder decodeFloatForKey:@"a2_a"]];
        _btBColor1 = [UIColor colorWithRed:[decoder decodeFloatForKey:@"b1_r"]
                                     green:[decoder decodeFloatForKey:@"b1_g"]
                                      blue:[decoder decodeFloatForKey:@"b1_b"]
                                     alpha:[decoder decodeFloatForKey:@"b1_a"]];
        _btBColor2 = [UIColor colorWithRed:[decoder decodeFloatForKey:@"b2_r"]
                                     green:[decoder decodeFloatForKey:@"b2_g"]
                                      blue:[decoder decodeFloatForKey:@"b2_b"]
                                     alpha:[decoder decodeFloatForKey:@"b2_a"]];
        _btCColor1 = [UIColor colorWithRed:[decoder decodeFloatForKey:@"c1_r"]
                                     green:[decoder decodeFloatForKey:@"c1_g"]
                                      blue:[decoder decodeFloatForKey:@"c1_b"]
                                     alpha:[decoder decodeFloatForKey:@"c1_a"]];
        _btCColor2 = [UIColor colorWithRed:[decoder decodeFloatForKey:@"c2_r"]
                                     green:[decoder decodeFloatForKey:@"c2_g"]
                                      blue:[decoder decodeFloatForKey:@"c2_b"]
                                     alpha:[decoder decodeFloatForKey:@"c2_a"]];
        _btDColor1 = [UIColor colorWithRed:[decoder decodeFloatForKey:@"d1_r"]
                                     green:[decoder decodeFloatForKey:@"d1_g"]
                                      blue:[decoder decodeFloatForKey:@"d1_b"]
                                     alpha:[decoder decodeFloatForKey:@"d1_a"]];
        _btDColor2 = [UIColor colorWithRed:[decoder decodeFloatForKey:@"d2_r"]
                                     green:[decoder decodeFloatForKey:@"d2_g"]
                                      blue:[decoder decodeFloatForKey:@"d2_b"]
                                     alpha:[decoder decodeFloatForKey:@"d2_a"]];
        
        _btAStylo = [decoder decodeIntForKey:@"btAStylo"];
        _btBStylo = [decoder decodeIntForKey:@"btBStylo"];
        _btCStylo = [decoder decodeIntForKey:@"btCStylo"];
        _btDStylo = [decoder decodeIntForKey:@"btDStylo"];
        _dpadStylo = [decoder decodeIntForKey:@"dpadStylo"];
        _gyroStylo = [decoder decodeIntForKey:@"gyroStylo"];
        _fundoStylo = [decoder decodeIntForKey:@"fundoStylo"];
        
        
        _LetraA = [decoder decodeIntForKey:@"LetraA"];
        _LetraS = [decoder decodeIntForKey:@"LetraS"];

        _LetraC = [UIColor colorWithRed:[decoder decodeFloatForKey:@"lc_r"]
                                     green:[decoder decodeFloatForKey:@"lc_g"]
                                      blue:[decoder decodeFloatForKey:@"lc_b"]
                                     alpha:[decoder decodeFloatForKey:@"lc_a"]];
        _LetraP = CGPointMake([decoder decodeFloatForKey:@"lpx"],[decoder decodeFloatForKey:@"lpy"]);
        
        
        
    }
    return self;
}
- (void)comitConfig {
    self.status++;
}
-(id) copyWithZone: (NSZone *) zone
{
    ConfigData* newConfig = [[ConfigData allocWithZone:zone] init];
    
    [newConfig setStatus:_status];
    [newConfig setMId:_mId];
    [newConfig setFundo:_Fundo];
    [newConfig setBtAColor1:_btAColor1];
    [newConfig setBtAColor2:_btAColor2];
 
    [newConfig setBtBColor1:_btBColor1];
    [newConfig setBtBColor2:_btBColor2];
    [newConfig setBtCColor1:_btCColor1];
    [newConfig setBtCColor2:_btCColor2];
    [newConfig setBtDColor1:_btDColor1];
    [newConfig setBtDColor2:_btDColor2];
    
    [newConfig setBtAlabel:_btAlabel];
    [newConfig setBtBlabel:_btBlabel];
    [newConfig setBtClabel:_btClabel];
    [newConfig setBtDlabel:_btDlabel];
    
    [newConfig setBtAHorizontalAlign:_btAHorizontalAlign];
    [newConfig setBtAVerticalAlign:_btAVerticalAlign];
    [newConfig setBtBHorizontalAlign:_btBHorizontalAlign];
    [newConfig setBtBVerticalAlign:_btBVerticalAlign];
    [newConfig setBtCHorizontalAlign:_btCHorizontalAlign];
    [newConfig setBtCVerticalAlign:_btCVerticalAlign];
    [newConfig setBtDHorizontalAlign:_btDHorizontalAlign];
    [newConfig setBtDVerticalAlign:_btDVerticalAlign];
    
    [newConfig setImgNode:_ImgNode];
    [newConfig setImgRect:_ImgRect];
    
    [newConfig setDPad:_dPad];
    [newConfig setBtA:_btA];
    [newConfig setBtB:_btB];
    [newConfig setBtC:_btC];
    [newConfig setBtD:_btD];
    [newConfig setBtGy:_btGy];
    
    [newConfig setBtAStylo:_btAStylo];
    [newConfig setBtBStylo:_btBStylo];
    [newConfig setBtCStylo:_btCStylo];
    [newConfig setBtDStylo:_btDStylo];
    [newConfig setDpadStylo:_dpadStylo];
    [newConfig setGyroStylo:_gyroStylo];
    [newConfig setFundoStylo:_fundoStylo];
    
    return newConfig;
}

@end;