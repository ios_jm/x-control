

#import "GameClientAppDelegate.h"
#include <arpa/inet.h>
@interface NSNetService (QNetworkAdditions)

- (BOOL)qNetworkAdditions_getInputStream:(out NSInputStream **)inputStreamPtr 
    outputStream:(out NSOutputStream **)outputStreamPtr;

@end

@implementation NSNetService (QNetworkAdditions)

- (BOOL)qNetworkAdditions_getInputStream:(out NSInputStream **)inputStreamPtr
    outputStream:(out NSOutputStream **)outputStreamPtr
{
    BOOL                result;
    CFReadStreamRef     readStream;
    CFWriteStreamRef    writeStream;

    result = NO;
    
    readStream = NULL;
    writeStream = NULL;
    
    if ( (inputStreamPtr != NULL) || (outputStreamPtr != NULL) ) {
        CFNetServiceRef     netService;

        netService = CFNetServiceCreate(
            NULL, 
            (__bridge CFStringRef) [self domain], 
            (__bridge CFStringRef) [self type], 
            (__bridge CFStringRef) [self name], 
            0
        );
        if (netService != NULL) {
            CFStreamCreatePairWithSocketToNetService(
                NULL, 
                netService, 
                ((inputStreamPtr  != nil) ? &readStream  : NULL), 
                ((outputStreamPtr != nil) ? &writeStream : NULL)
            );
            CFRelease(netService);
        }
    
        
        result = ! ((( inputStreamPtr != NULL) && ( readStream == NULL)) || 
                    ((outputStreamPtr != NULL) && (writeStream == NULL)));
    }
    if (inputStreamPtr != NULL) {
        *inputStreamPtr  = CFBridgingRelease(readStream);
    }
    if (outputStreamPtr != NULL) {
        *outputStreamPtr = CFBridgingRelease(writeStream);
    }
    
    return result;
}

@end

#pragma mark -
#pragma mark ClientAppDelegate class

@interface GameClientAppDelegate () <NSNetServiceBrowserDelegate, NSStreamDelegate>







@property (nonatomic, strong, readwrite) NSNetServiceBrowser *  serviceBrowser;
@property (nonatomic, strong, readwrite) NSInputStream *        inputStream;
@property (nonatomic, strong, readwrite) NSOutputStream *       outputStream;
@property (nonatomic, strong, readwrite) NSMutableData *        inputBuffer;
@property (nonatomic, strong, readwrite) NSMutableData *        outputBuffer;



- (void)closeStreams;

@end

@implementation GameClientAppDelegate

@synthesize services = _serviceList;

@synthesize serviceBrowser = _serviceBrowser;
@synthesize inputStream  = _inputStream;
@synthesize outputStream = _outputStream;
@synthesize inputBuffer  = _inputBuffer;
@synthesize outputBuffer = _outputBuffer;

- (id)init
{
    self = [super init];
    if (self) {
        [self load];
        _JoyI = [[ImageData alloc] init];
        _joyS = [[ServerData alloc] init];
        _JoySqueue = [[HsuQueue alloc] init];
        _CFGSqueue = [[HsuQueue alloc] init];

    }
    return self;
}

- (void)load {
    self.conectado = false;
    self.serviceBrowser = [[NSNetServiceBrowser alloc] init];
    self.services = [[NSMutableArray alloc] init];
    [self.serviceBrowser setDelegate:self];
    _config_status = 0;
    [self.serviceBrowser searchForServicesOfType:@"_xcontroler._tcp." inDomain:@""];
}

#pragma mark -
#pragma mark NSNetServiceBrowser delegate methods


- (void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didFindService:(NSNetService *)aNetService moreComing:(BOOL)moreComing {
    #pragma unused(aNetServiceBrowser)
    #pragma unused(moreComing)
    if (![self.services containsObject:aNetService]) {
        [self willChangeValueForKey:@"services"];
        [self.services addObject:aNetService];
        [self didChangeValueForKey:@"services"];
        if ([self.delegate respondsToSelector:@selector(Conection:)]) {
            [self.delegate Conection:YES];
        }
    }
}

- (void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didRemoveService:(NSNetService *)aNetService moreComing:(BOOL)moreComing {
    #pragma unused(aNetServiceBrowser)
    #pragma unused(moreComing)
    if ([self.services containsObject:aNetService]) {
        [self willChangeValueForKey:@"services"];
        [self.services removeObject:aNetService];
        [self didChangeValueForKey:@"services"];
        if ([self.delegate respondsToSelector:@selector(Conection:)]) {
            [self.delegate Conection:YES];
        }
    }
}

#pragma mark -
#pragma mark Stream methods

- (void)openStreamsToNetService:(NSNetService *)netService {
    NSInputStream * istream;
    NSOutputStream * ostream;

    [self closeStreams];

    if ([netService qNetworkAdditions_getInputStream:&istream outputStream:&ostream]) {
        self.inputStream = istream;
        self.outputStream = ostream;
        [self.inputStream  setDelegate:self];
        [self.outputStream setDelegate:self];
        [self.inputStream  scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [self.outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [self.inputStream  open];
        [self.outputStream open];
        
    }
}

- (void)closeStreams {
    [self.inputStream  setDelegate:nil];
    [self.outputStream setDelegate:nil];
    [self.inputStream  close];
    [self.outputStream close];
    [self.inputStream  removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [self.outputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    self.inputStream  = nil;
    self.outputStream = nil;
    self.inputBuffer  = nil;
    self.outputBuffer = nil;
}

- (void)startOutput
{
    assert([self.outputBuffer length] != 0);
    
    NSInteger actuallyWritten = [self.outputStream write:[self.outputBuffer bytes] maxLength:[self.outputBuffer length]];
    if (actuallyWritten > 0) {
        [self.outputBuffer replaceBytesInRange:NSMakeRange(0, (NSUInteger) actuallyWritten) withBytes:NULL length:0];
    } else {
        [self CloseAll];
    }
}

- (void)outputText:(NSString *)text
{
    NSData * dataToSend = [text dataUsingEncoding:NSUTF8StringEncoding];
    if (self.outputBuffer != nil) {
        BOOL wasEmpty = ([self.outputBuffer length] == 0);
        [self.outputBuffer appendData:dataToSend];
        if (wasEmpty) {
            [self startOutput];
        }
    }
}
- (void)CloseAll {
    NSLog(@"disconect");
    [self closeStreams];
    [[self services] removeAllObjects];
    if ([self.delegateCena respondsToSelector:@selector(Conection:)]) {
        [self.delegateCena Conection:NO];
    }
}
- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)streamEvent {

    assert(aStream == self.inputStream || aStream == self.outputStream);
    if (aStream.streamStatus == NSStreamStatusClosed) {
        [self CloseAll];
        return;
    }
    switch(streamEvent) {
        case NSStreamEventOpenCompleted: {
            if (aStream == self.inputStream) {
                self.inputBuffer = [[NSMutableData alloc] init];
            } else {
                self.outputBuffer = [[NSMutableData alloc] init];
            }
        } break;
        case NSStreamEventHasSpaceAvailable: {
            if ([self.outputBuffer length] != 0) {
                [self startOutput];
            }
        } break;
        case NSStreamEventHasBytesAvailable: {
            uint8_t buffer[1025];
            NSMutableData* gData = [[NSMutableData alloc] init];
            while ([[self inputStream] hasBytesAvailable]) {
                NSInteger actuallyRead = [self.inputStream read:(uint8_t *)buffer maxLength:sizeof(buffer)];
                if (actuallyRead > 0) {
                    //NSLog(@"leu %d",actuallyRead);
                    [gData appendBytes:buffer length:actuallyRead];
                    
                }
            }
            //NSLog(@"leu %d",gData.length);
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self processaServerData:[gData gunzippedData]];
            });

            
            
        } break;
        case NSStreamEventErrorOccurred: {
            [self CloseAll];
        } break;
        case NSStreamEventEndEncountered: {
            [self CloseAll];
        } break;
            //case nsstreamEvent
        default:
            break;
    }
}
#pragma mark processas
- (void) processaServerData:(NSData*)sv {
    if (!sv) return;
    @try {
        NSDictionary* qData = [NSKeyedUnarchiver unarchiveObjectWithData:sv];
        if ([qData objectForKey:@"config"]) {
            ConfigData* newConfig = [qData objectForKey:@"config"];
            NSLog(@"recebendo %d",newConfig.status);
            [[self CFGSqueue] enqueue:newConfig];
        }
        if ([qData objectForKey:@"dado"]) {
            ServerData* nSrdata = [qData objectForKey:@"dado"];
            [[self JoySqueue] enqueue:nSrdata];
        }
        if ([qData objectForKey:@"imagem"]) {
            //NSLog(@"chegou imagem");
            ImageData* tmp = [qData objectForKey:@"imagem"];
            if (_JoyI.status<tmp.status) {
                ImageData* nIgdata = [qData objectForKey:@"imagem"];
                
                _JoyI = nIgdata;
                //NSURL* url = [NSURL URLWithString:[tmp img]];
                //[self LoadImg:url];
                NSData* data = [[tmp img] dataUsingEncoding:NSUTF8StringEncoding];
                UIImage* io = [UIImage imageWithData:data];
                [[self JoyI] setImage:io];
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [[self delegateCena] setConfig:YES];
                });

            }
        }
    }
    @catch (NSException *exception) {
    }
    @finally {
    }
}

#pragma mark -
#pragma mark download
- (void)LoadImg:(NSURL*)url {
    NSURLRequest *theRequest=[NSURLRequest requestWithURL:url
                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                          timeoutInterval:60.0];
    _receivedData = [NSMutableData dataWithCapacity: 0];
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    if (!theConnection) {
        _receivedData = nil;
    }
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [_receivedData setLength:0];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_receivedData appendData:data];
}
- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
    //theConnection = nil;
    _receivedData = nil;
    
    // inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{

    if (_receivedData)
    {
        UIImage* ui = [[UIImage alloc] initWithData:_receivedData];
        
        [[self JoyI] setImage:ui];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [[self delegateCena] setConfig:YES];
        });
    }

    _receivedData = nil;
}
#pragma mark conectar
- (void)conectar:(int)index {
    if (self.services.count >= 1) {
    NSNetService * selectedService = [self.services objectAtIndex:(NSUInteger) index];
        
        _url = @" ";
        [self openStreamsToNetService:selectedService];
        [[self serviceBrowser] stop];
    }
}
- (void)enviarGd:(GameData *)gData {
    [gData setConfig:[self config_status]];
    NSData *aData = [NSKeyedArchiver archivedDataWithRootObject:gData];
    if (self.outputBuffer != nil) {
        BOOL wasEmpty = ([self.outputBuffer length] == 0);
        [self.outputBuffer appendData:aData];
        if (wasEmpty) {
            [self startOutput];
        }
    }
    
}
- (NSString *)getStringFromAddressData:(NSData *)dataIn {
    struct sockaddr_in  *socketAddress = nil;
    NSString            *ipString = nil;
    
    socketAddress = (struct sockaddr_in *)[dataIn bytes];
    ipString = [NSString stringWithFormat: @"%s",
                inet_ntoa(socketAddress->sin_addr)]; 
    return ipString;
}

@end
