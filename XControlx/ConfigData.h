//
//  ConfigData.h
//  xControler
//
//  Created by Tiago Paluch on 11/09/14.
//  Copyright (c) 2014 Tiago Paluch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>

@interface ConfigData : NSObject <NSCopying>
@property (strong) NSString* mId;
@property int status;

@property CGRect dPad;
@property CGRect btA;
@property CGRect btB;
@property CGRect btC;
@property CGRect btD;
@property CGRect btGy;

@property UIColor* dPadColor1;
@property UIColor* dPadColor2;
@property UIColor* btAColor1;
@property UIColor* btAColor2;
@property UIColor* btBColor1;
@property UIColor* btBColor2;
@property UIColor* btCColor1;
@property UIColor* btCColor2;
@property UIColor* btDColor1;
@property UIColor* btDColor2;
@property UIColor* Fundo;



@property NSString* btAlabel;
@property NSString* btBlabel;
@property NSString* btClabel;
@property NSString* btDlabel;

@property CGRect ImgRect;
@property SKNode* ImgNode;

@property int btAVerticalAlign;
@property int btAHorizontalAlign;
@property int btBVerticalAlign;
@property int btBHorizontalAlign;
@property int btCVerticalAlign;
@property int btCHorizontalAlign;
@property int btDVerticalAlign;
@property int btDHorizontalAlign;

@property int btAStylo;
@property int btBStylo;
@property int btCStylo;
@property int btDStylo;
@property int dpadStylo;
@property int gyroStylo;
@property int fundoStylo;

@property UIColor* LetraC;
@property CGPoint LetraP;
@property int LetraA;
@property int LetraS;

- (void) comitConfig;
-(id) copyWithZone: (NSZone *) zone;
@end
