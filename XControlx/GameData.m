//
//  GameData.m
//  ServidorControle
//
//  Created by Tiago Paluch on 24/07/14.
//  Copyright (c) 2014 Tiago Paluch. All rights reserved.
//

#import "GameData.h"

@implementation GameData

- (instancetype)init
{
    self = [super init];
    if (self) {
        _Config = 0;
    }
    return self;
}
- (void) encodeWithCoder:(NSCoder *)encoder {
    
    
    [encoder encodeFloat:_dpAngle  forKey:@"dpAngle"];
    [encoder encodeFloat:_dpVel.x forKey:@"dpVelx"];
    [encoder encodeFloat:_dpVel.y forKey:@"dpVely"];
    
    [encoder encodeBool:_botA forKey:@"botA"];
    [encoder encodeBool:_botB forKey:@"botB"];
    [encoder encodeBool:_botC forKey:@"botC"];
    [encoder encodeBool:_botD forKey:@"botD"];
    
    [encoder encodeFloat:_acc_x  forKey:@"acc_x"];
    [encoder encodeFloat:_acc_y  forKey:@"acc_y"];
    [encoder encodeFloat:_acc_z  forKey:@"acc_z"];
    [encoder encodeFloat:_rot_x  forKey:@"rot_x"];
    [encoder encodeFloat:_rot_y  forKey:@"rot_y"];
    [encoder encodeFloat:_rot_z  forKey:@"rot_z"];
    
    [encoder encodeFloat:_mag  forKey:@"mag"];
    [encoder encodeFloat:_tre  forKey:@"tre"];
    [encoder encodeFloat:_lat  forKey:@"lat"];
    [encoder encodeFloat:_log  forKey:@"log"];
    
    [encoder encodeInt:_Config forKey:@"Config"];
    [encoder encodeObject:_chat forKey:@"chat"];
    
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self) {
        float dpvelx;
        float dpvely;
        float dpAngleF;
        
        dpAngleF = [decoder decodeFloatForKey:@"dpAngle"];
        _dpAngle = dpAngleF;
        
        dpvelx = [decoder decodeFloatForKey:@"dpVelx"];
        dpvely = [decoder decodeFloatForKey:@"dpVely"];
        
        _dpVel = CGPointMake(dpvelx, dpvely);
        
        _botA = [decoder decodeBoolForKey:@"botA"];
        _botB = [decoder decodeBoolForKey:@"botB"];
        _botC = [decoder decodeBoolForKey:@"botC"];
        _botD = [decoder decodeBoolForKey:@"botD"];
        
        
        _acc_x = [decoder decodeFloatForKey:@"acc_x"];
        _acc_y = [decoder decodeFloatForKey:@"acc_y"];
        _acc_z = [decoder decodeFloatForKey:@"acc_z"];
        _rot_x = [decoder decodeFloatForKey:@"rot_x"];
        _rot_y = [decoder decodeFloatForKey:@"rot_y"];
        _rot_z = [decoder decodeFloatForKey:@"rot_z"];
        
        _mag = [decoder decodeFloatForKey:@"mag"];
        _tre = [decoder decodeFloatForKey:@"tre"];
        _lat = [decoder decodeFloatForKey:@"lat"];
        _log = [decoder decodeFloatForKey:@"log"];
        
        _Config = [decoder decodeIntForKey:@"Config"];
        _chat = [decoder decodeObjectForKey:@"chat"];
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    GameData* gd = [[GameData allocWithZone:zone] init];
    
    [gd setAcc_x:_acc_x];
    [gd setAcc_y:_acc_y];
    [gd setAcc_z:_acc_z];
    
    [gd setRot_x:_rot_x];
    [gd setRot_y:_rot_y];
    [gd setRot_z:_rot_y];
    
    [gd setBotA:_botA];
    [gd setBotB:_botB];
    [gd setBotC:_botC];
    [gd setBotD:_botD];
    
    [gd setConfig:_Config];
    [gd setDpAngle:_dpAngle];
    [gd setDpVel:_dpVel];
    
    return gd;
}
@end
